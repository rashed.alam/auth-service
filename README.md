**Instructions**  
Clone the projct

To run with docker simple enter the following command on the root directory  
**Docker compose up**

Run without docker  
**npm run dev**

For testing run  
**npm run test**

To build run  
**npm run build**


The project will run on **http://localhost:5000**


There are three API's


**POST** http://localhost:5000/api/auth/login (public)

**Payload**: {
    email: “nibir@gmail.com”,
    password: “123456” 		
}

**GET** http://localhost:5000/api/profile/detail (private - need auth)
Need to add authorization on the header
Authorization: token

**GET** http://localhost:5000/api/products/list (public)


There are only three valid users on the system.

```
[
    {
        id: 1,
        name: "Nibir",
        email: "nibir@gmail.com",
        password: "123456"
    },
    {
        id: 2,
        name: "Jon",
        email: "jon@gmail.com",
        password: "123456"
    },
    {
        id: 3,
        name: "Rob",
        email: "rob@gmail.com",
        password: "123456"
    }
]
```


