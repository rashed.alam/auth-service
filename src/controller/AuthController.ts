import { signToken } from '@server/utils/jwt';
import { findUser } from '@server/model/userModel';
import { ILoginReq, TypedRequestLoginBody } from '@server/routes/api/auth';

class AuthController {
    async login(req: TypedRequestLoginBody<ILoginReq>, res: any): Promise<void> {
        const { email, password } = req.body

        if (email && password) {
            const user = findUser(req.body)
            if (user) {
                const accessToken = await signToken({ email: email })
                return res.json({ accessToken: accessToken })
            }

            return res.status(404).json({ msg: "Wrong email or password" })
        } else {
            return res.status(403).json({ msg: "Email or password not provided" })
        }
    }
}

export default AuthController