import { productList } from "@server/dataModel/products";

class ProductController {
    list(req: any, res: any): void {
        const request = req;
        const response = res
        const products = productList

        response.json(products)
    }
}

export default ProductController
