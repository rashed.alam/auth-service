class UserController {
    async getProfile(req: any, res: any): Promise<void> {
        res.json(req.user);
    }
}

export default UserController
