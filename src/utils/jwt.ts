import jwt from 'jsonwebtoken';
import config from '@config/app';
import { ITokenPayload } from '@dataModel/user';

export const signToken = async (data: { email: string }): Promise<string> => {

  return jwt.sign({ email: data.email }, config.accessTokenPrivateKey, {
    algorithm: "HS256",
    expiresIn: config.accessTokenExpiresIn,
  })
}

export const verifyToken = async (token: string): Promise<ITokenPayload> => {
  return jwt.verify(token, config.accessTokenPrivateKey) as ITokenPayload
}
