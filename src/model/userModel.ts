import { IUser, userList } from '@server/dataModel/user';
import { ILoginReq } from '@routes/api/auth';

const findUser = (data: ILoginReq): IUser => {
    let foundUser = null;

    userList.forEach((user) => {
        if (data.email == user.email && data.password == user.password) {
            foundUser = user
        }
    })

    return foundUser
}

const findByEmail = (email: string): IUser => {
    let foundUser = null;

    userList.forEach((user) => {
        if (user.email == email) {
            foundUser = user
        }
    })

    return foundUser
}

export { findUser, findByEmail }