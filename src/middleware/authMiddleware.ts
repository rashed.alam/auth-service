import { ITokenPayload } from '@dataModel/user';
import { findByEmail } from '@server/model/userModel';
import { verifyToken } from '@server/utils/jwt';
class AuthMiddleware {
    async authenticate(req: any, res: any, next: any): Promise<void> {

        const token = req.headers.authorization

        try {
            const tokenPayload: ITokenPayload = await verifyToken(token)

            const user = findByEmail(tokenPayload.email)
            req.user = user;
            next()

        } catch (error) {
            console.log(error)
            return res.status(401).json({ msg: "Authentication Failed" })
        }
    }
}

export default AuthMiddleware
