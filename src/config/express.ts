import express from 'express';
import router from "@server/routes/index"

const createServer = (): express.Application => {
  const app = express();

  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());

  app.disable('x-powered-by');

  app.use(router);

  return app;
};

export { createServer };
