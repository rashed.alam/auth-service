import express from 'express';
import API from '@routes/api/index'

const router = express.Router();

router.use('/api', API);

export default router