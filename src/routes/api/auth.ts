import express from 'express';
import { Request, Response } from 'express';

import AuthController from '@server/controller/AuthController';

export interface ILoginReq {
    email: string,
    password: string
}

export interface TypedRequestLoginBody<ILoginReq> extends Request {
    body: ILoginReq
}

const router = express.Router();

const authController = new AuthController()

router.post('/login', (req: TypedRequestLoginBody<ILoginReq>, res: Response) => authController.login(req, res));

export default router