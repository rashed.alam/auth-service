import express from 'express';
import UserController from "@controller/UserController";
import AuthMiddleware from "@middleware/authMiddleware";

const userController = new UserController()
const authMiddleware = new AuthMiddleware()

const router = express.Router();

router.get('/detail', authMiddleware.authenticate, userController.getProfile);

export default router