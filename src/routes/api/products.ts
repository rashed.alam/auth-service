import express from 'express';
import ProductController from "@server/controller/ProductController";

const router = express.Router();

const productController = new ProductController()

router.get('/list', productController.list);

export default router