import express from 'express';
import Profile from './profile'
import Products from './products'
import Auth from "./auth";

const router = express.Router();

router.use('/products', Products);
router.use('/profile', Profile);
router.use('/auth', Auth);

export default router