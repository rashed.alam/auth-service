interface IUser {
    id: number,
    name: string,
    email: string,
    password: string
}

interface ITokenPayload {
    email: string,
    iat: number,
    exp: number
}

const userList: IUser[] = [
    {
        id: 1,
        name: "Nibir",
        email: "nibir@gmail.com",
        password: "123456"
    },
    {
        id: 2,
        name: "Jon",
        email: "jon@gmail.com",
        password: "123456"
    },
    {
        id: 3,
        name: "Rob",
        email: "rob@gmail.com",
        password: "123456"
    }
]

export { IUser, userList, ITokenPayload }
