interface IProduct {
    id: number,
    name: string,
    price: number
}

const productList: IProduct[] = [
    {
        id: 1,
        name: "Car",
        price: 10000
    },
    {
        id: 2,
        name: "Bike",
        price: 20000
    },
    {
        id: 3,
        name: "Mobile",
        price: 30000
    }
]

export { IProduct, productList }
