process.env.PORT = '3001';
import { findByEmail, findUser } from '@server/model/userModel';
import { signToken, verifyToken } from '@server/utils/jwt';
import express from 'express';
import { Server } from 'net';

console.log('process.version', process.version);

describe('Index', () => {
  it('should work', async () => {
    const listen = jest.spyOn(Server.prototype, 'listen');
    jest.mock('@config/express', () => ({
      createServer: jest.fn().mockReturnValue(express()),
    }));
    await import('@server/index');
    expect(listen).toBeCalled();
    const server = listen.mock.results[0].value as Server;
    setImmediate(() => {
      server.close();
    });
    listen.mockRestore();
  });
});

describe('JWT token test', () => {

  it('new acces token should be return on signToken', async () => {
    const token = await signToken({ email: 'nibir@gmail.com' })
    expect(typeof token).toBe('string');
  });

  it('token should be validated and return extracted payload', async () => {
    const token = await signToken({ email: 'nibir@gmail.com' })
    const payload = await verifyToken(token)
    expect(payload).toHaveProperty('email')
    expect(payload).toHaveProperty('iat')
    expect(payload).toHaveProperty('exp')
    expect(payload).toHaveProperty('email', 'nibir@gmail.com')
  });
});

describe('User model testing', () => {

  it('user with given email and pasword should match ', () => {
    const expected = {
      id: 1,
      name: "Nibir",
      email: "nibir@gmail.com",
      password: "123456"
    }

    const actual = findUser({ email: 'nibir@gmail.com', password: '123456' })
    expect(actual).toMatchObject(expected)
  });

  it('user with given email should return userdata', () => {
    const expected = {
      id: 1,
      name: "Nibir",
      email: "nibir@gmail.com",
      password: "123456"
    }

    const actual = findByEmail('nibir@gmail.com')
    expect(actual).toMatchObject(expected)
  });

  it('if user did not foud null value should return', () => {
    const actual = findByEmail('test@test.com')
    expect(actual).toBeNull()
  });
});