import { createServer } from '@server/config/express';
import { productList } from '@server/dataModel/products';
import { signToken } from '@server/utils/jwt';
import supertest from 'supertest';

const app = createServer();
describe('Product', () => {
    it('Product list should be returned', async () => {
        const expected = productList
        const res = await supertest(app)
            .get('/api/products/list')

        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject(expected)
    });
});

describe('Profile', () => {
    it('User profile data should return', async () => {
        const token = await signToken({ email: 'nibir@gmail.com' })

        const expected = {
            id: 1,
            name: "Nibir",
            email: "nibir@gmail.com",
            password: "123456"
        }

        const res = await supertest(app)
            .get('/api/profile/detail')
            .set('authorization', token)

        expect(res.body).toMatchObject(expected)
        expect(res.statusCode).toEqual(200)
    });

    it('when authorization header is not set or invalid should return 401 response', async () => {
        const res = await supertest(app)
            .get('/api/profile/detail')

        expect(res.statusCode).toEqual(401)
    });
});

describe('Login', () => {
    it('should receive access token', async () => {
        const res = await supertest(app)
            .post('/api/auth/login')
            .send({
                email: "nibir@gmail.com",
                password: "123456"
            })

        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('accessToken')
    });

    it('if email or password filed does not provide return status should be 403', async () => {
        const res = await supertest(app)
            .post('/api/auth/login')
            .send({
                email: "nibir@gmail.com"
            })

        expect(res.statusCode).toEqual(403)
        expect(res.body).toHaveProperty('msg')
        expect(res.body.msg).toBe('Email or password not provided')
    });

    it('if email or password is wrong returned status code should be 404', async () => {
        const res = await supertest(app)
            .post('/api/auth/login')
            .send({
                email: "nibir1@gmail.com",
                password: "111",
            })

        expect(res.statusCode).toEqual(404)
        expect(res.body).toHaveProperty('msg')
        expect(res.body.msg).toBe('Wrong email or password')
    });
});


