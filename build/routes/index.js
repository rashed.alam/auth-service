"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var express_1 = tslib_1.__importDefault(require("express"));
var index_1 = tslib_1.__importDefault(require("@routes/api/index"));
var router = express_1.default.Router();
router.use('/api', index_1.default);
exports.default = router;
//# sourceMappingURL=index.js.map