"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var express_1 = tslib_1.__importDefault(require("express"));
var profile_1 = tslib_1.__importDefault(require("./profile"));
var products_1 = tslib_1.__importDefault(require("./products"));
var auth_1 = tslib_1.__importDefault(require("./auth"));
var router = express_1.default.Router();
router.use('/products', products_1.default);
router.use('/profile', profile_1.default);
router.use('/auth', auth_1.default);
exports.default = router;
//# sourceMappingURL=index.js.map