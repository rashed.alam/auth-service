"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var express_1 = tslib_1.__importDefault(require("express"));
var AuthController_1 = tslib_1.__importDefault(require("@server/controller/AuthController"));
var router = express_1.default.Router();
var authController = new AuthController_1.default();
router.post('/login', function (req, res) { return authController.login(req, res); });
exports.default = router;
//# sourceMappingURL=auth.js.map