"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var express_1 = tslib_1.__importDefault(require("express"));
var ProductController_1 = tslib_1.__importDefault(require("@server/controller/ProductController"));
var router = express_1.default.Router();
var productController = new ProductController_1.default();
router.get('/list', productController.list);
exports.default = router;
//# sourceMappingURL=products.js.map