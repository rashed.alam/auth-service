"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var express_1 = tslib_1.__importDefault(require("express"));
var UserController_1 = tslib_1.__importDefault(require("@controller/UserController"));
var authMiddleware_1 = tslib_1.__importDefault(require("@middleware/authMiddleware"));
var userController = new UserController_1.default();
var authMiddleware = new authMiddleware_1.default();
var router = express_1.default.Router();
router.get('/detail', authMiddleware.authenticate, userController.getProfile);
exports.default = router;
//# sourceMappingURL=profile.js.map