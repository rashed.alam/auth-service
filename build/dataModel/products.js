"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.productList = void 0;
var productList = [
    {
        id: 1,
        name: "Car",
        price: 10000
    },
    {
        id: 2,
        name: "Bike",
        price: 20000
    },
    {
        id: 3,
        name: "Mobile",
        price: 30000
    }
];
exports.productList = productList;
//# sourceMappingURL=products.js.map