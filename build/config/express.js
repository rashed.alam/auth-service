"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
var tslib_1 = require("tslib");
var express_1 = tslib_1.__importDefault(require("express"));
var index_1 = tslib_1.__importDefault(require("@server/routes/index"));
var createServer = function () {
    var app = express_1.default();
    app.use(express_1.default.urlencoded({ extended: false }));
    app.use(express_1.default.json());
    app.disable('x-powered-by');
    app.use(index_1.default);
    return app;
};
exports.createServer = createServer;
//# sourceMappingURL=express.js.map