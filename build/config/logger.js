"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
var tslib_1 = require("tslib");
var winston_1 = tslib_1.__importDefault(require("winston"));
var LoggerWrapper = function () {
    return winston_1.default.createLogger({
        transports: [new winston_1.default.transports.Console()],
        exitOnError: false,
    });
};
exports.logger = LoggerWrapper();
//# sourceMappingURL=logger.js.map