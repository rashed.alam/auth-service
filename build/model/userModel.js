"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findByEmail = exports.findUser = void 0;
var user_1 = require("@server/dataModel/user");
var findUser = function (data) {
    var foundUser = null;
    user_1.userList.forEach(function (user) {
        if (data.email == user.email && data.password == user.password) {
            foundUser = user;
        }
    });
    return foundUser;
};
exports.findUser = findUser;
var findByEmail = function (email) {
    var foundUser = null;
    user_1.userList.forEach(function (user) {
        if (user.email == email) {
            foundUser = user;
        }
    });
    return foundUser;
};
exports.findByEmail = findByEmail;
//# sourceMappingURL=userModel.js.map