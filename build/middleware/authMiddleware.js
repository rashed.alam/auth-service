"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var userModel_1 = require("@server/model/userModel");
var jwt_1 = require("@server/utils/jwt");
var AuthMiddleware = /** @class */ (function () {
    function AuthMiddleware() {
    }
    AuthMiddleware.prototype.authenticate = function (req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var token, tokenPayload, user, error_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = req.headers.authorization;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, jwt_1.verifyToken(token)];
                    case 2:
                        tokenPayload = _a.sent();
                        user = userModel_1.findByEmail(tokenPayload.email);
                        req.user = user;
                        next();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [2 /*return*/, res.status(401).json({ msg: "Authentication Failed" })];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return AuthMiddleware;
}());
exports.default = AuthMiddleware;
//# sourceMappingURL=authMiddleware.js.map