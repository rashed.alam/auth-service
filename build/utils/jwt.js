"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyToken = exports.signToken = void 0;
var tslib_1 = require("tslib");
var jsonwebtoken_1 = tslib_1.__importDefault(require("jsonwebtoken"));
var app_1 = tslib_1.__importDefault(require("@config/app"));
var signToken = function (data) { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
    return tslib_1.__generator(this, function (_a) {
        return [2 /*return*/, jsonwebtoken_1.default.sign({ email: data.email }, app_1.default.accessTokenPrivateKey, {
                algorithm: "HS256",
                expiresIn: app_1.default.accessTokenExpiresIn,
            })];
    });
}); };
exports.signToken = signToken;
var verifyToken = function (token) { return tslib_1.__awaiter(void 0, void 0, void 0, function () {
    return tslib_1.__generator(this, function (_a) {
        return [2 /*return*/, jsonwebtoken_1.default.verify(token, app_1.default.accessTokenPrivateKey)];
    });
}); };
exports.verifyToken = verifyToken;
//# sourceMappingURL=jwt.js.map