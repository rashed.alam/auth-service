"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var moduleAlias = tslib_1.__importStar(require("module-alias"));
var sourcePath = process.env.NODE_ENV === 'development' ? 'src' : 'build';
moduleAlias.addAliases({
    '@server': sourcePath,
    '@config': sourcePath + "/config",
    '@domain': sourcePath + "/domain",
});
var express_1 = require("@config/express");
var http_1 = tslib_1.__importDefault(require("http"));
var logger_1 = require("@config/logger");
var host = process.env.HOST || '0.0.0.0';
var port = process.env.PORT || '5000';
function startServer() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var app, server, signalTraps;
        var _this = this;
        return tslib_1.__generator(this, function (_a) {
            app = express_1.createServer();
            server = http_1.default.createServer(app).listen({ host: host, port: port }, function () {
                var addressInfo = server.address();
                logger_1.logger.info("Server ready at http://" + addressInfo.address + ":" + addressInfo.port);
            });
            signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2'];
            signalTraps.forEach(function (type) {
                process.once(type, function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    return tslib_1.__generator(this, function (_a) {
                        logger_1.logger.info("process.once " + type);
                        server.close(function () {
                            logger_1.logger.debug('HTTP server closed');
                        });
                        return [2 /*return*/];
                    });
                }); });
            });
            return [2 /*return*/];
        });
    });
}
startServer();
//# sourceMappingURL=index.js.map