"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var jwt_1 = require("@server/utils/jwt");
var userModel_1 = require("@server/model/userModel");
var AuthController = /** @class */ (function () {
    function AuthController() {
    }
    AuthController.prototype.login = function (req, res) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, email, password, user, accessToken;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = req.body, email = _a.email, password = _a.password;
                        if (!(email && password)) return [3 /*break*/, 3];
                        user = userModel_1.findUser(req.body);
                        if (!user) return [3 /*break*/, 2];
                        return [4 /*yield*/, jwt_1.signToken({ email: email })];
                    case 1:
                        accessToken = _b.sent();
                        return [2 /*return*/, res.json({ accessToken: accessToken })];
                    case 2: return [2 /*return*/, res.status(404).json({ msg: "Wrong email or password" })];
                    case 3: return [2 /*return*/, res.status(403).json({ msg: "Email or password not provided" })];
                }
            });
        });
    };
    return AuthController;
}());
exports.default = AuthController;
//# sourceMappingURL=AuthController.js.map