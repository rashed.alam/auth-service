"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var products_1 = require("@server/dataModel/products");
var ProductController = /** @class */ (function () {
    function ProductController() {
    }
    ProductController.prototype.list = function (req, res) {
        var request = req;
        var response = res;
        var products = products_1.productList;
        response.json(products);
    };
    return ProductController;
}());
exports.default = ProductController;
//# sourceMappingURL=ProductController.js.map